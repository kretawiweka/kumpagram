import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Skeleton } from 'antd';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';

import fetchData from 'utils/fetchData';
import { BASE_API } from 'constants/index';

const Post = () => {
  const [data, setData] = useState(null);
  const [isLoading, setIsloading] = useState(true);
  const [isOpenLightbox, setIsOpenLightbox] = useState(false);
  const [photoIndex, setPhotoIndex] = useState(0);
  const { albumId } = useParams();

  const toggleLightbox = index => {
    setIsOpenLightbox(!isOpenLightbox);
    setPhotoIndex(index);
  };

  useEffect(() => {
    const getPhotoOnAlbum = async () => {
      const photoOnAlbum = await fetchData({
        url: `${BASE_API}/photos?albumId=${albumId}`
      }).catch(err => {
        throw err;
      });
      console.log(photoOnAlbum.data);
      setData(photoOnAlbum.data);
      setIsloading(false);
    };
    getPhotoOnAlbum();
  }, [albumId]);

  return (
    <React.Fragment>
      <h3 style={{ textAlign: 'center' }}>Photo</h3>
      {isLoading ? (
        <Skeleton />
      ) : (
        data.map((item, index) => (
          <img
            alt={item.title}
            key={index}
            src={item.thumbnailUrl}
            onClick={() => {
              toggleLightbox(index);
            }}
          />
        ))
      )}
      {isOpenLightbox && (
        <Lightbox
          mainSrc={data[photoIndex].url}
          nextSrc={data[(photoIndex + 1) % data.length]}
          prevSrc={data[(photoIndex + data.length - 1) % data.length]}
          onCloseRequest={toggleLightbox}
          onMovePrevRequest={() =>
            setPhotoIndex((photoIndex + data.length - 1) % data.length)
          }
          onMoveNextRequest={() =>
            setPhotoIndex((photoIndex + 1) % data.length)
          }
        />
      )}
    </React.Fragment>
  );
};

export default Post;
