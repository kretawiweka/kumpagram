import React, { useEffect, useState } from 'react';
import { Card, Skeleton } from 'antd';
import { Link, useParams } from 'react-router-dom';
import { Button } from 'antd';

import fetchData from 'utils/fetchData';
import { BASE_API } from 'constants/index';
import isEmpty from 'utils/isEmpty';

const UserAlbum = () => {
  const [data, setData] = useState(null);
  const [isLoading, setIsloading] = useState(true);
  const { userId } = useParams();

  useEffect(() => {
    const getAlbumData = async () => {
      const albumData = await fetchData({
        url: `${BASE_API}/albums/?${userId}`
      }).catch(err => {
        throw err;
      });
      setData(albumData.data);
      setIsloading(false);
    };
    getAlbumData();
  }, [userId]);

  return (
    <React.Fragment>
      <h3 style={{ textAlign: 'center' }}>Album</h3>
      {isLoading ? (
        <Skeleton />
      ) : (
        !isEmpty(data) &&
        data.map((item, index) => (
          <Link key={index} to={`/album/${item.id}`}>
            <Card
              key={index}
              style={{ margin: '14px 0px' }}
              bodyStyle={{ padding: '14px' }}
            >
              <h4>Album {item.title}</h4>
              <Button block>Photo Album</Button>
            </Card>
          </Link>
        ))
      )}
    </React.Fragment>
  );
};

export default UserAlbum;
