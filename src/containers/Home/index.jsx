import React, { useEffect, useState } from 'react';
import { Card, Skeleton, Icon } from 'antd';
import { Link } from 'react-router-dom';
import { Button } from 'antd';

import fetchData from 'utils/fetchData';
import { BASE_API } from 'constants/index';
import isEmpty from 'utils/isEmpty';

const Home = () => {
  const [data, setData] = useState(null);
  const [isLoading, setIsloading] = useState(true);
  useEffect(() => {
    const getUserData = async () => {
      const userData = await fetchData({
        url: `${BASE_API}/users`
      }).catch(err => {
        throw err;
      });
      setData(userData.data);
      setIsloading(false);
    };
    getUserData();
  }, []);
  return (
    <React.Fragment>
      {isLoading ? (
        <Skeleton />
      ) : (
        !isEmpty(data) &&
        data.map((item, index) => (
          <Card
            key={index}
            style={{ margin: '14px 0px' }}
            title={`${item.name} (${item.username})`}
          >
            <Card.Grid hoverable={false} style={{ width: '100%' }}>
              <p>
                <Icon type="mail" theme="twoTone" /> {item.email}
              </p>
              <p>
                <Icon type="phone" theme="twoTone" /> {item.phone}
              </p>
              <p>
                <Icon type="profile" theme="twoTone" /> {item.website}
              </p>
            </Card.Grid>
            <Card.Grid hoverable={false} style={{ width: '100%' }}>
              <strong>Address</strong>
              <p>{`${item.address.street}, ${item.address.suite}, ${item.address.city}, ${item.address.zipcode}`}</p>
            </Card.Grid>
            <Card.Grid hoverable={false} style={{ width: '100%' }}>
              <strong>Company</strong>
              <p>{item.company.name}</p>
            </Card.Grid>
            <Button.Group style={{ width: '100%', display: 'flex' }}>
              <Link to={`/user-album/${item.id}`} style={{ flex: 1 }}>
                <Button size="large" style={{ width: '100%' }}>
                  <Icon type="pic-center" />
                  Album
                </Button>
              </Link>
              <Link to={`/user-post/${item.id}`} style={{ flex: 1 }}>
                <Button size="large" type="primary" style={{ width: '100%' }}>
                  <Icon type="message" />
                  Post
                </Button>
              </Link>
            </Button.Group>
          </Card>
        ))
      )}
    </React.Fragment>
  );
};

export default Home;
