import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Card, Skeleton, Icon, message } from 'antd';

import fetchData from 'utils/fetchData';
import { BASE_API } from 'constants/index';
import FloatButton from 'components/Button/FloatButton';
import ModalFormComment from 'components/ModalForm/Comment';
import { WrapperContent } from './style';

const Post = () => {
  const [data, setData] = useState(null);
  const [isLoading, setIsloading] = useState(true);

  const [isOpenModal, setIsOpenModal] = useState(false);
  const [commentData, setCommentData] = useState({});
  const [isCreateMode, setIsCreateMode] = useState(true);

  const { postId } = useParams();

  useEffect(() => {
    const getCommentOnPost = async () => {
      const commentOnPost = await fetchData({
        url: `${BASE_API}/comments?postId=${postId}`
      }).catch(err => {
        throw err;
      });
      setData(commentOnPost.data);
      setIsloading(false);
    };
    getCommentOnPost();
  }, [postId]);

  const toggleModal = () => {
    setIsOpenModal(!isOpenModal);
  };

  const hanldeChange = event => {
    setCommentData({
      ...commentData,
      [event.target.name]: event.target.value
    });
  };

  const handleSubmit = async event => {
    event.preventDefault();
    const METHOD = isCreateMode ? 'POST' : 'PUT';
    const URL = isCreateMode
      ? `${BASE_API}/comments/`
      : `${BASE_API}/comments/${commentData.id}`;
    const res = await fetchData({
      url: URL,
      method: METHOD,
      data: {
        id: commentData.id,
        title: commentData.title,
        body: commentData.body,
        postId: postId
      }
    }).catch(err => {
      message.error('Something error');
      throw err;
    });
    if (res.status === 200 || res.status === 201) {
      message.success(isCreateMode ? 'Comment Created' : 'Comment Updated');
    } else {
      message.error('Something error');
    }
  };

  const createComment = () => {
    setIsCreateMode(true);
    setCommentData({});
    setIsOpenModal(true);
  };

  const editComment = data => {
    setIsCreateMode(false);
    setCommentData(data);
    setIsOpenModal(true);
  };

  const deleteComment = async idPost => {
    const res = await fetchData({
      url: `${BASE_API}/posts/${idPost}`,
      method: 'DELETE'
    }).catch(err => {
      message.error('Something error, comment cannot delete');
      throw err;
    });
    if (res.status === 200 || res.status === 201) {
      message.success('Comment deleted');
    } else {
      message.error('Something error, comment cannot delete');
    }
  };

  return (
    <React.Fragment>
      <h3 style={{ textAlign: 'center' }}>Comment</h3>
      <ModalFormComment
        isOpen={isOpenModal}
        toggleFunc={toggleModal}
        handleChangeFunc={hanldeChange}
        data={commentData}
        handleSubmitFunc={handleSubmit}
        isCreateMode={isCreateMode}
      />
      <WrapperContent>
        {isLoading ? (
          <Skeleton />
        ) : (
          data.map((item, index) => (
            <Card
              key={index}
              style={{ margin: '14px 0px' }}
              bodyStyle={{ padding: '14px' }}
              actions={[
                <Icon
                  onClick={() => {
                    editComment(item);
                  }}
                  type="edit"
                  key="edit"
                />,
                <Icon
                  onClick={() => {
                    deleteComment(item.id);
                  }}
                  type="delete"
                  key="delete"
                />
              ]}
            >
              <strong>{item.title}</strong>
              <Icon type="mail" theme="twoTone" /> {item.email}
              <p>{item.body}</p>
            </Card>
          ))
        )}
      </WrapperContent>
      <FloatButton handleClickFunc={createComment}>Create Comment</FloatButton>
    </React.Fragment>
  );
};

export default Post;
