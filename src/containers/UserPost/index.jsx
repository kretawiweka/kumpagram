import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Card, Skeleton, Icon, message } from 'antd';

import FloatButton from 'components/Button/FloatButton';
import ModalFormPost from 'components/ModalForm/Post';
import fetchData from 'utils/fetchData';
import { BASE_API } from 'constants/index';
import { WrapperContent } from './style';

const UserPost = () => {
  const [data, setData] = useState(null);
  const [isLoading, setIsloading] = useState(true);
  const { userId } = useParams();

  const [isOpenModal, setIsOpenModal] = useState(false);
  const [postData, setPostData] = useState({});
  const [isCreateMode, setIsCreateMode] = useState(true);

  const history = useHistory();

  useEffect(() => {
    const getPostUser = async () => {
      const postUserData = await fetchData({
        url: `${BASE_API}/posts?userId=${userId}`
      }).catch(err => {
        throw err;
      });
      setData(postUserData.data);
      setIsloading(false);
    };
    getPostUser();
  }, [userId]);

  const toggleModal = () => {
    setIsOpenModal(!isOpenModal);
  };

  const hanldeChange = event => {
    setPostData({
      ...postData,
      [event.target.name]: event.target.value
    });
  };

  const handleSubmit = async event => {
    event.preventDefault();
    const METHOD = isCreateMode ? 'POST' : 'PUT';
    const URL = isCreateMode
      ? `${BASE_API}/posts/`
      : `${BASE_API}/posts/${postData.id}`;
    const res = await fetchData({
      url: URL,
      method: METHOD,
      data: {
        id: postData.id,
        title: postData.title,
        body: postData.body,
        userId: userId
      }
    }).catch(err => {
      message.error('Something error');
      throw err;
    });
    if (res.status === 200 || res.status === 201) {
      message.success(isCreateMode ? 'Post Created' : 'Post Updated');
    } else {
      message.error('Something error');
    }
  };

  const createPost = () => {
    setIsCreateMode(true);
    setPostData({});
    setIsOpenModal(true);
  };

  const editPost = data => {
    setIsCreateMode(false);
    setPostData(data);
    setIsOpenModal(true);
  };

  const deletePost = async idComment => {
    const res = await fetchData({
      url: `${BASE_API}/posts/${idComment}`,
      method: 'DELETE'
    }).catch(err => {
      message.error('Something error, post cannot delete');
      throw err;
    });
    if (res.status === 200 || res.status === 201) {
      message.success('Post deleted');
    } else {
      message.error('Something error, post cannot delete');
    }
  };

  return (
    <React.Fragment>
      <h3 style={{ textAlign: 'center' }}>Post</h3>
      <ModalFormPost
        isOpen={isOpenModal}
        toggleFunc={toggleModal}
        handleChangeFunc={hanldeChange}
        data={postData}
        handleSubmitFunc={handleSubmit}
        isCreateMode={isCreateMode}
      />
      <WrapperContent>
        {isLoading ? (
          <Skeleton />
        ) : (
          data.map((item, index) => (
            <Card
              key={index}
              style={{ margin: '14px 0px' }}
              bodyStyle={{ padding: '14px' }}
              actions={[
                <Icon
                  onClick={() => {
                    history.push(`/post/${item.id}`);
                  }}
                  type="file-search"
                  key="detail"
                />,
                <Icon
                  onClick={() => {
                    editPost(item);
                  }}
                  type="edit"
                  key="edit"
                />,
                <Icon
                  onClick={() => {
                    deletePost(item.id);
                  }}
                  type="delete"
                  key="delete"
                />
              ]}
            >
              <strong>{item.title}</strong>
              <p>{item.body}</p>
            </Card>
          ))
        )}
      </WrapperContent>
      <FloatButton handleClickFunc={createPost}>Create Post</FloatButton>
    </React.Fragment>
  );
};

export default UserPost;
