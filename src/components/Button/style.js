import styled from 'styled-components';

const ButtonWrapper = styled.div`
  padding: 14px;
  background-color: #ffffff;
  border: 1px solid #eaebec;
  position: fixed;
  max-width: 480px;
  margin: -15px;
  width: 100%;
  bottom: 14px;
`;

export { ButtonWrapper };
