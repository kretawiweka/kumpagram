import React from 'react';
import { Button } from 'antd';
import PropTypes from 'prop-types';

import { ButtonWrapper } from './style';

const FloatButton = props => {
  return (
    <ButtonWrapper>
      <Button onClick={props.handleClickFunc} type="primary" block>
        {props.children}
      </Button>
    </ButtonWrapper>
  );
};

FloatButton.propTypes = {
  handleClickFunc: PropTypes.func.isRequired
};

export default FloatButton;
