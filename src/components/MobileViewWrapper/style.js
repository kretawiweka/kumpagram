import styled from 'styled-components';

const MobileWrapper = styled.div`
  padding: 14px;
  background-color: #ffffff;
  border: 1px solid #f5f5f5;
  max-width: 480px;
  margin: auto;
  min-height: calc(100vh - 70px);
`;

export { MobileWrapper };
