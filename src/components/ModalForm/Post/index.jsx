import React from 'react';
import { Modal, Form, Input, Button } from 'antd';
import PropTypes from 'prop-types';

import isEmpty from 'utils/isEmpty';

const ModalFormPost = props => {
  console.log(props.data);
  return (
    <Modal
      title="Kumpagram Post"
      visible={props.isOpen}
      footer={[
        <Button key="close" onClick={props.toggleFunc}>
          Close
        </Button>
      ]}
    >
      <Form onSubmit={props.handleSubmitFunc}>
        <Form.Item label="ID">
          <Input
            name="id"
            value={isEmpty(props.data) ? '' : props.data.id}
            onChange={props.handleChangeFunc}
            required
          />
        </Form.Item>
        <Form.Item label="Title">
          <Input.TextArea
            name="title"
            value={isEmpty(props.data) ? '' : props.data.title}
            onChange={props.handleChangeFunc}
            required
          />
        </Form.Item>
        <Form.Item label="Body">
          <Input.TextArea
            name="body"
            value={isEmpty(props.data) ? '' : props.data.body}
            onChange={props.handleChangeFunc}
            required
          />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" block>
            {props.isCreateMode ? 'Create' : 'Update'}
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

ModalFormPost.propTypes = {
  data: PropTypes.object,
  isOpen: PropTypes.bool.isRequired,
  toggleFunc: PropTypes.func.isRequired,
  handleChangeFunc: PropTypes.func.isRequired,
  isCreateMode: PropTypes.bool.isRequired
};

export default ModalFormPost;
