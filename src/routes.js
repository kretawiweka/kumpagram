import Home from 'pages/home';
import UserPost from 'pages/user-post';
import PostDetail from 'pages/post';
import UserAlbum from 'pages/user-album';
import Album from 'pages/album';

const routes = [
  {
    path: '/',
    page: Home
  },
  {
    path: '/user-post/:userId',
    page: UserPost
  },
  {
    path: '/post/:postId',
    page: PostDetail
  },
  {
    path: '/user-album/:userId',
    page: UserAlbum
  },
  {
    path: '/album/:albumId',
    page: Album
  }
];

export default routes;
