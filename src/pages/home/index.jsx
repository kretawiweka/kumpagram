import React from 'react';
import { PageHeader } from 'antd';

import Home from 'containers/Home';
import MobileViewWrapper from 'components/MobileViewWrapper';

const HomePage = () => {
  return (
    <React.Fragment>
      <PageHeader
        style={{
          border: '1px solid rgb(235, 237, 240)',
          backgroundColor: '#ffffff'
        }}
        title="Kumpagram"
      />
      <MobileViewWrapper>
        <Home />
      </MobileViewWrapper>
    </React.Fragment>
  );
};

export default HomePage;
