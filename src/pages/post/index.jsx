import React from 'react';
import { PageHeader } from 'antd';
import { useHistory } from 'react-router-dom';

import Post from 'containers/Post';
import MobileViewWrapper from 'components/MobileViewWrapper';

const PostPage = () => {
  const history = useHistory();

  return (
    <React.Fragment>
      <PageHeader
        style={{
          border: '1px solid rgb(235, 237, 240)',
          backgroundColor: '#ffffff'
        }}
        title="Kumpagram"
        onBack={() => {
          history.goBack();
        }}
      />
      <MobileViewWrapper>
        <Post />
      </MobileViewWrapper>
    </React.Fragment>
  );
};

export default PostPage;
