import React from 'react';
import { PageHeader } from 'antd';
import { useHistory } from 'react-router-dom';

import UserAlbum from 'containers/UserAlbum';
import MobileViewWrapper from 'components/MobileViewWrapper';

const UserAlbumPage = () => {
  const history = useHistory();

  return (
    <React.Fragment>
      <PageHeader
        style={{
          border: '1px solid rgb(235, 237, 240)',
          backgroundColor: '#ffffff'
        }}
        title="Kumpagram"
        onBack={() => {
          history.push('/');
        }}
      />
      <MobileViewWrapper>
        <UserAlbum />
      </MobileViewWrapper>
    </React.Fragment>
  );
};

export default UserAlbumPage;
