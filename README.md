# Kumpagram

Project boilerplate from [craft-cra](https://github.com/kretawiweka/craft-cra) (crafting and building cra for me and other) [Create React App](https://github.com/facebook/create-react-app).

## Development

### Project Setup

```
git clone https://kretawiweka@bitbucket.org/kretawiweka/kumpagram.git

yarn install
```

### Local Development

```
yarn start
```

### Test

```
yarn test
```

### Build for Production

```
yarn build
```
